The Broadway Boogie uses six envelopes, defined as follows:
```
    2ENVELOPE1,0,0,0,0,0,0,0,126,-4,-2,-4,50,60
    3  ENVELOPE2,0,0,0,0,0,0,0,126,-4,-3,-3,126,100  
    4ENVELOPE3,1,0,0,0,0,0,0,63,43,0,-63,63,126 
    5ENVELOPE4,1,0,0,0,0,0,0,63,43,0,-63,63,100
11010 ENVELOPE1,1,0,0,0,0,0,0,126,-10,-5,-2,110,100
11020 ENVELOPE2,3,0,0,0,0,0,0,126,-8,-4,-1,126,100 
11030 ENVELOPE3,1,0,0,0,0,0,0,126,-8,-4,-1,126,126
```

![waveforms](envelopes.png)

 * 2: dies away in 0.3s, 20% volume
 * 3: dies away in 0.3s, 100% volume
 * 4: constant tone, 100% volume
 * 5: constant tone, 50% volume.
 * 11010: dies away in 0.25s, 65% volume.
 * 11020: dies away in 0.85s, 100% volume
 * 11030: dies away in 0.35s, 100% volume

I suspect 0.25 to 0.35 are all 0.3 but have
experimental error.

100% = veloc 100
 65% = veloc 85
 50% = veloc 70
 20% = veloc 50

0.3s = dur 0.1
0.8s = dur 3.3
