from midi_writer import MIDIFile, Notes
import pitches
import copy

BPM = 100 # == original

NOTE_PLAY_LENGTH_COEFFICIENT = 1
NOTE_FULL_LENGTH_COEFFICIENT = 4/48

OCTAVE = 12*4

SECTIONS = {
        'piano intro': 4150,
        'trumpet solo': 10000,
        'piano solo': 12000,
        'bass solo': 13500,
        'finale': 16500,
        }

MIDI_PIANO = 4
MIDI_BASS = 40
MIDI_TRUMPET = 66 # which is actually a sax, but I think it sounds better
MIDI_SYNTH = 81

MIDI_INSTRUMENT_NUMBER = {
        'piano': MIDI_PIANO,
        'bass': MIDI_BASS,
        'trumpet': MIDI_TRUMPET,
        'synth': MIDI_SYNTH,
        }

NULL_ENVELOPE    = { 'length':  0.0,  'volume':   0, }
LINE_2           = { 'length':  0.3,  'volume':  50, }
LINE_3           = { 'length':  0.3,  'volume': 100, }
LINE_4           = { 'length': None,  'volume': 100, }
LINE_5           = { 'length': None,  'volume':  70, }
LINE_11010       = { 'length': 10.2,  'volume':  85, }
LINE_11010       = { 'length': 13.3,  'volume': 100, }
LINE_11010       = { 'length': 10.2,  'volume': 100, }
WHITE_NOISE_HIGH = { 'length': 2,     'volume': 100, 'voice': 'cymbal',
                    'pitch_override': 84, }
WHITE_NOISE_MID  = { 'length': 2,     'volume': 100, 'voice': 'snare',
                    'pitch_override': 70, }
WHITE_NOISE_LOW  = { 'length': 2,     'volume': 100, 'voice': 'drum',
                    'pitch_override': 74,
                    }

INSTRUMENTS = [
        MIDI_PIANO ,
        MIDI_PIANO ,
        MIDI_TRUMPET,
        MIDI_BASS,
        ]

CHANNELS = {
        0: MIDI_PIANO,
        1: MIDI_TRUMPET,
        2: MIDI_BASS,
        3: MIDI_SYNTH,
        }

VOICES = {
        'piano right': {'track': 0, 'channel': 0, 'instrument': MIDI_PIANO, },
        'piano left':  {'track': 1, 'channel': 0, 'instrument': MIDI_PIANO, },
        'trumpet':     {'track': 2, 'channel': 1, 'instrument': MIDI_TRUMPET, },
        'bass':        {'track': 3, 'channel': 2, 'instrument': MIDI_BASS, },
        'synth':       {'track': 4, 'channel': 3, 'instrument': MIDI_SYNTH, },
        'drums':       {'track': 5, 'channel': 9, 'instrument': None, },
        'silent':      {'track': 0, 'channel': 0, 'instrument': None, },
        }

TURN_DOWN_TRUMPET_AND_BASS = 0.8 # they drown out the piano

END_MARKER = 256

LOG_HEADER = (
        '.---------- Midi ------------------.-------- Beeb ----.----- Channel pos -----.\n'
        '|Ch Trk Pitch Play Full Vol Voice  | Ch Env Pitch Len |      0    1    2    3 |'
        )
LOG_FORMAT = '|%2s %2s %5s %4.1f %4.1f %3s %-7s |%3s %3s %5s %3s |%*s%7.1f'

DIVIDER = '-'*55

with open('boogie.bas', 'rb') as f:
    source = [line.decode('ascii').strip() for line in f.readlines()
              if b'CHR$' not in line # avoid inline control codes
              ]

class Note:
    def __init__(self,
                 beeb_channel, envelope,
                 beeb_pitch, beeb_length,
                 ):
        self.beeb_channel = beeb_channel
        self.envelope = envelope
        assert isinstance(envelope, dict)
        self.beeb_pitch = beeb_pitch
        self.beeb_length = beeb_length

    @property
    def midi_pitch(self):

        if 'pitch_override' in self.envelope:
            pitch = self.envelope['pitch_override']
        else:
            pitch = self.beeb_pitch+OCTAVE

        if pitch in pitches.PITCHES:
            return pitches.PITCHES[pitch]

        # not in PITCHES; must be a blue note
        notes = [
                (abs(pitch-beeb), midi)
                for beeb, midi in pitches.PITCHES.items()]
        return min(notes)[1]

    @property
    def midi_play_length(self):
        v = self.envelope['length']
        if v is None:
            result = self.beeb_length/10 # why /10?
        else:
            result = v

        return result * NOTE_PLAY_LENGTH_COEFFICIENT

    @property
    def midi_full_length(self):
        return self.beeb_length * NOTE_FULL_LENGTH_COEFFICIENT

    @property
    def _voice_details(self):
        key = self.envelope['voice']
        if key in ('cymbal', 'snare', 'drum'):
            key = 'drums'
        elif key is None:
            key = 'silent'
        elif key=='piano':
            key += ' '+self.envelope['hand']
        return VOICES[key]

    @property
    def midi_channel(self):
        return self._voice_details['channel']

    @property
    def midi_track(self):
        return self._voice_details['track']

    @property
    def midi_volume(self):
        result = self.envelope['volume']

        if self.envelope['voice'] in ('trumpet', 'bass'):
            result = int(result*TURN_DOWN_TRUMPET_AND_BASS)

        return result

    def __str__(self):
        return (
                f'SOUND {self.beeb_channel},{self.envelope},'
                f'{self.beeb_pitch},{self.beeb_length}'
                )

    __repr__ = __str__

def read_from_data(starting_line):
    numbers = []
    found = False
    starting_line = str(starting_line)
    for line in source:
        if not found:
            if not line.startswith(starting_line):
                continue
            found = True

        if 'DATA' not in line:
            break

        values = line.split('DATA')
        if len(values)==1:
            continue
        numbers.extend(
                [int(x) for x in
                 values[1].split(',')
                 ])

        if END_MARKER in numbers:
            p = numbers.index(END_MARKER)

            numbers = numbers[:p]
            break

    return numbers

class Song:
    def __init__(self, midi):
        self.midi = midi

        channel_count = len(set(
            [voice['channel'] for voice in VOICES.values()]))
        track_count = len(set(
            [voice['channel'] for voice in VOICES.values()]))

        self.channel_pos = [0.0] * channel_count

        self.envelopes = {}

        # per the BBC User Guide, on P(itch) for channel 0:
        #
        # When P is set to 3 or 7 then the frequency of the noise is
        # controlled by the pitch setting of sound channel number 1.
        self.most_recent_pitch_on_channel_1 = None

        # workaround for a bug in midi_writer
        self.used_notes = set()

        self.hands_are_swapped = False
        
        for channel, instrument in CHANNELS.items():
            self.midi.add_tempo(channel, 0, BPM)

        for voice_details in VOICES.values():
            if voice_details['instrument'] is not None:
                self.midi.addProgramChange(
                        tracknum = voice_details['track'],
                        channel = voice_details['channel'],
                        time = 0,
                        program = voice_details['instrument']-1,
                        )

        for track in range(track_count):
            self.midi.addTimeSignature(track, 0,
                                       numerator=4,
                                       denominator=2, # this means 4/4. Why?
                                       clocks_per_tick=24,
                                       )
 
        for i, name in enumerate(VOICES):
            self.midi.addTrackName(
                    i,
                    time=0,
                    trackName=name,
                    )

        print(LOG_HEADER)

    def set_envelope(self, number, details, voice):
        self.envelopes[number] = details
        self.envelopes[number]['voice'] = voice

    def SOUND(self, channel, env, pitch, duration):

        if env<0:
            envelope = {
                    'length': None,
                    'volume': round((100/-15)*env),
                    'voice': 'synth',
                    }
        elif env==0:
            envelope = NULL_ENVELOPE
        else:
            envelope = copy.copy(self.envelopes[env])

        if channel==0:
            if pitch==3:
                envelope['voice'] = 'bass'
                envelope['pitch_override'] = (
                        # Why 4? idk
                        self.most_recent_pitch_on_channel_1+4
                        )
            elif pitch==4:
                envelope = WHITE_NOISE_HIGH
            elif pitch==5:
                envelope = WHITE_NOISE_MID
            elif pitch==6:
                envelope = WHITE_NOISE_LOW
            else:
                raise ValueError(f"Unimplemented noise: {pitch}")

        if envelope['voice']=='piano':
            right_hand = (channel==1)
            
            if self.hands_are_swapped:
                right_hand = not right_hand

            if right_hand:
                envelope['hand'] = 'right'
            else:
                envelope['hand'] = 'left'

        note = Note(channel, envelope, pitch, duration)

        midi_play_length = note.midi_play_length
        midi_full_length = note.midi_full_length

        voice_name = envelope['voice']
        if voice_name is None:
            voice_name = 'silent'

        print(LOG_FORMAT % (
                note.midi_channel,
                note.midi_track,
                note.midi_pitch,
                midi_play_length,
                midi_full_length,
                note.midi_volume,
                voice_name,

                note.beeb_channel,
                env,
                note.beeb_pitch,
                note.beeb_length,

                note.beeb_channel*5,
                '',
                self.channel_pos[note.beeb_channel],
                ))

        volume = note.midi_volume

        if note.beeb_channel==1:
            self.most_recent_pitch_on_channel_1 = note.beeb_pitch

        this_note = (self.channel_pos[note.beeb_channel], note.midi_pitch)

        if note.beeb_pitch==0:
            envelope = NULL_ENVELOPE
        elif midi_play_length==0:
            pass
        elif this_note in self.used_notes:
            print("---used")
        else:
            self.midi.add_note(
                    note.midi_track,
                    note.midi_channel,
                    note.midi_pitch,
                    self.channel_pos[note.beeb_channel],
                    midi_play_length,
                    volume,
                    )
            self.used_notes.add(this_note)

        self.channel_pos[note.beeb_channel] += midi_full_length

    def play_section(self, name):

        line = SECTIONS[name]
        print(f"{DIVIDER} {name}: RESTORE {line}")

        if name=='piano intro':
            block_size = 3
        else:
            block_size = 4

        bass_adjust = (name=='bass solo')

        self.hands_are_swapped = (name=='trumpet solo')

        for channel, env, pitch, length in self._read_from_DATA(
                read_from_data(line),
                block_size = block_size,
                ):

            if bass_adjust and channel==1:
                self.SOUND(channel, 0, pitch-4, length)
                channel = 0
                pitch = 3

            self.SOUND(channel, env, pitch, length)

    def flush_buffers(self):
        """
        *FX 21
        """
        self.channel_pos = [max(self.channel_pos)]*len(self.channel_pos)
        print(f'{DIVIDER} *FX 21: sync to {round(self.channel_pos[0], 2)}')

    def _read_from_DATA(self,
                        numbers,
                        block_size,
                        ):
        result = []
        while numbers:
            if block_size==3:
                channel, pitch, duration = numbers[:3]
                if channel==0:
                    envelope=0
                else:
                    envelope=2
            else:
                channel, envelope, pitch, duration = numbers[:4]

            yield (channel, envelope, pitch, duration)
            numbers = numbers[block_size:]

        return result

    def intro(self):
        print(f"{DIVIDER} intro")
        for hi_hat in range(4):
            self.SOUND(0,1,4,6)
            self.SOUND(0,1,4,4)
            self.SOUND(0,1,4,2)
            self.SOUND(1,0,0,12)
            self.SOUND(2,0,0,12)
            self.SOUND(3,0,0,12)
 
    def outro(self):
        print(f"{DIVIDER} outro")
        self.SOUND(0,2,4,76)
        for x in range(149, 5, -4):
            self.SOUND(1,-15,x,2)
            self.SOUND(2,2,x,2)
            self.SOUND(3,4,x,2)
        self.SOUND(0,1,4,4)
        self.SOUND(0,1,4,4)
        self.SOUND(1,0,5,10)
        self.SOUND(2,0,69,10)
        self.SOUND(3,0,93,10)
        self.SOUND(0,2,4,20)
        self.SOUND(1,4,5,48)
        self.SOUND(2,4,69,48)
        self.SOUND(3,4,93,48)

def main():
    boogie = Song(
            midi = MIDIFile(len(VOICES)),
            )

    boogie.set_envelope(0, NULL_ENVELOPE, None)
    boogie.set_envelope(1, LINE_2, 'piano')
    boogie.set_envelope(2, LINE_3, 'piano')
    boogie.set_envelope(3, LINE_4, 'trumpet')
    boogie.set_envelope(4, LINE_5, 'piano')

    boogie.intro()
    boogie.play_section('piano intro')
    boogie.play_section('trumpet solo')

    boogie.set_envelope(3, LINE_4, 'piano')

    boogie.play_section('piano solo')

    boogie.flush_buffers() # *FX21
    boogie.play_section('bass solo')

    boogie.flush_buffers() # *FX21
    # the original pads it out by 2 here, but this is a kludge
    boogie.play_section('finale')

    boogie.outro()

    print(boogie.channel_pos)

    with open("boogie.mid", "wb") as output_file:
        boogie.midi.write_file(output_file)

if __name__=='__main__':
    main()
