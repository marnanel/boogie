This script converts the BBC Micro demo "Broadway Boogie"
into MIDI.

It requires [FlipperPA's `midi-writer` library](https://github.com/FlipperPA/midi-writer).

Links:
 * [A video demo.](https://gitlab.com/marnanel/boogie)
 * [An overview of the envelopes.](envelopes.md)
 * [The original BASIC version.](boogie.bas)
 * [A BBC micro disk image with the BASIC version.](boogie.ssd)
