SOUNDFONT = /etc/alternatives/default-GM.sf2 

.PRECIOUS: boogie.mid

boogie.wav: boogie.mid
	fluidsynth $(SOUNDFONT) $< -F boogie.wav

boogie.mid: boogie.py
	python boogie.py
